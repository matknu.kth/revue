// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { collection, doc, initializeFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD6WM_zSeNvsbGkBRgvjJiUGh6cKTX-qvs",
  authDomain: "revue-avega.firebaseapp.com",
  projectId: "revue-avega",
  storageBucket: "revue-avega.appspot.com",
  messagingSenderId: "336870556097",
  appId: "1:336870556097:web:880dccb535441836bcaf40",
};
const firebaseConfigTest = {
  apiKey: "AIzaSyCTr9Y_OjlMvFz7TlRztjRo81DF5_AwkvE",
  authDomain: "revue-avega-test.firebaseapp.com",
  projectId: "revue-avega-test",
  storageBucket: "revue-avega-test.appspot.com",
  messagingSenderId: "19280761053",
  appId: "1:19280761053:web:41af9b7fea4afaab144b34",
};
const config =
  location.host === "revue-avega.web.app" ? firebaseConfig : firebaseConfigTest;

// Initialize Firebase
const app = initializeApp(config);
export const db = initializeFirestore(app, {
  ignoreUndefinedProperties: true,
});
const analytics = getAnalytics(app);

// export const sessionsRef = collection(db, "session2022");
// export const sessionRef = (sessionId: string) =>
//   doc(db, "session2022", sessionId);
// export const mesRef = collection(db, "me2022");

export const modulesRef = collection(db, "module");
export const moduleRef = (id: string) => doc(db, "module", id);
export const questionsRef = (moduleId: string) =>
  collection(db, "module", moduleId, "questions");
export const answersRef = collection(db, "answer");
export const usersRef = collection(db, "users");
export const adminRef = (email: string) => doc(db, "admin", email);
