import { createApp } from "vue";
import { createPinia } from "pinia";
import * as Sentry from "@sentry/vue";
import { BrowserTracing } from "@sentry/tracing";
import { CaptureConsole } from "@sentry/integrations";

import App from "./App.vue";
import router from "./router";

// import './assets/main.css'
import "./validationRules";
import "./firebase";

const app = createApp(App).use(createPinia()).use(router);

Sentry.init({
  app,
  dsn: "https://87aec7e922e949f59051738ca6763a72@o4504808362082304.ingest.sentry.io/4504808363524096",
  integrations: [
    new BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
      tracePropagationTargets: [
        "localhost",
        "revue-avega.web.app",
        "revue-avega-test.web.app",
        /^\//,
      ],
    }),
    new CaptureConsole({
      levels: ["error"],
    }),
  ],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

app.mount("#app");
